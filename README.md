# Duolingo

**Unofficial** library for interacting with the Duolingo APIs.

This project is forked from https://github.com/KartikTalwar/Duolingo, which appears to be abandoned. I forked the project because of recent changes by Duolingo to their APIs have broken that copy of the library. I have pared down some functionality to just the most useful and most tested features of that original library.

## Disclaimer

I barely use this library myself. Accordingly:

  * This project is not published to PyPi. You can't install it using `pip`. I use this library for a personal project that doesn't involve downloading requirements from PyPi, and don't want the responsibility of publishing it there.
  * It takes me a long time to spin up on merge requests. Expect a month or more for a response and six months or more for resolution.
  * My focus is on maintaining the existing functionality of this library: I will probably not accept requests for new features, and may or may not accept merge requests with new features.

If you've got a more intensive use case for this library, I would encourage you to fork it and I'd be glad to hand over ownership!

## Usage

See [USAGE.md](USAGE.md)

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

